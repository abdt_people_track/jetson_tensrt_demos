"""trt_yolo.py

This script demonstrates how to do real-time object detection with
TensorRT optimized YOLO engine.
"""


import os
import time
import argparse

import cv2
import pycuda.autoinit  # This is needed for initializing CUDA driver
import numpy as np
from utils.yolo_classes import get_cls_dict
from utils.camera import add_camera_args, Camera
from utils.display import open_window, set_display, show_fps
from utils.visualization import BBoxVisualization

from utils.yolo_with_plugins import TrtYOLO
import sort


WINDOW_NAME = "TrtYOLODemo"


def parse_args():
    """Parse input arguments."""
    desc = "Capture and display live camera video, while doing " "real-time object detection with TensorRT optimized " "YOLO model on Jetson"
    parser = argparse.ArgumentParser(description=desc)
    parser = add_camera_args(parser)
    parser.add_argument("-c", "--category_num", type=int, default=80, help="number of object categories [80]")
    parser.add_argument(
        "-m", "--model", type=str, required=True, help=("[yolov3|yolov3-tiny|yolov3-spp|yolov4|yolov4-tiny]-" "[{dimension}], where dimension could be a single " "number (e.g. 288, 416, 608) or WxH (e.g. 416x256)")
    )
    args = parser.parse_args()
    return args


# def draw_door(frame, doors):
#     for d in doors:
#         point1 = (int(d[0]), int(d[1]))
#         point2 = (int(d[2]), int(d[3]))
#         frame = cv2.rectangle(frame, point1, point2, (0, 255, 0), thickness=2)
#     return frame


def image_detection(image, trt_yolo, conf_th):
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_rgb = cv2.cvtColor(image_gray, cv2.COLOR_GRAY2RGB)
    boxes, confs, clss = boxes, confs, clss = trt_yolo.detect(image_rgb, conf_th)
    return boxes, confs, clss


# def prepare_to_tracking(frame, trt_yolo):
#     boxes, confs, clss = image_detection(frame, trt_yolo)
#     doors = []
#     persons = []
#     for box, conf, cl in zip(boxes, confs, clss):
#         # print(cl)
#         if str(cl) == str(1.0):
#             # doors.append(self.bbox2points(self.resize_bbox(box))
#             doors.append(box)
#         elif str(cl) == str(0.0):
#             persons.append((box, conf, cl))
#     if len(persons) != 0:
#         detects = np.zeros((len(persons), 5))
#         for i, (box, conf, cl) in enumerate(persons):
#             confidence = float(conf / 100)  # [0;1]
#             # bbox = self.resize_bbox(box)
#             # x1, y1, x2, y2 = self.bbox2points(bbox)
#             x1, y1, x2, y2 = box[0], box[1], box[2], box[3]
#             detects[i, :] = np.array([x1, y1, x2, y2, confidence])
#     else:
#         detects = np.empty((0, 5))
#     # frame = cv2.resize(frame, (self.out_width, self.out_height), interpolation=cv2.INTER_LINEAR)
#     # print(detects.shape)
#     return detects, doors, frame


# def track_and_draw_and_write(trackers, frame, out):
#     for d in trackers:
#         point1 = (int(d[0]), int(d[1]))
#         point2 = (int(d[2]), int(d[3]))
#         id_track = int(d[4])
#         center_point = self.calc_center_bbox(point1, point2)
#         if id_track not in self.dic:
#             self.dic[id_track] = None
#         self.counter(id_track, center_point[1])
#         result_img = cv2.rectangle(frame, point1, point2, (255, 0, 0), thickness=2)
#         result_img = cv2.circle(result_img, center_point, 2, (255, 0, 0), thickness=5)
#         result_img = cv2.line(result_img, (0, self.up), (self.out_width, self.up), (255, 0, 0))
#         result_img = cv2.line(result_img, (0, self.down), (self.out_width, self.down), (255, 0, 0))
#         result_img = cv2.putText(result_img, "{}".format(id_track), (int(d[0]), int(d[1]) - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
#         result_img = cv2.rectangle(result_img, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
#         result_img = cv2.putText(result_img, f"income: {self.income}", (self.out_width - 100, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
#         result_img = cv2.putText(result_img, f"Outcome: {self.outcome}", (self.out_width - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
#     if len(trackers) != 0:
#         # pass
#         print(result_img.shape, "res_img")
#         out.write(result_img)
#         # cv2.imshow("bus", result_img)
#     else:
#         # pass
#         print(frame.shape, "frame_sh")
#         frame = cv2.rectangle(frame, (self.out_width - 120, 5), (self.out_width, 65), (255, 255, 255), -1)
#         frame = cv2.putText(frame, f"income: {self.income}", (self.out_width - 100, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
#         frame = cv2.putText(frame, f"Outcome: {self.outcome}", (self.out_width - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
#         out.write(frame)
#         # cv2.imshow("bus", frame)
#     return None


def loop_and_detect(cam, trt_yolo, tracker, conf_th, vis):
    """Continuously capture images from camera and do object detection.

    # Arguments
      cam: the camera instance (video source).
      trt_yolo: the TRT YOLO object detector instance.
      conf_th: confidence/score threshold for object detection.
      vis: for visualization.
    """
    full_scrn = False
    start_time = time.time()
    count_of_frames = 0
    fps = 0.0
    tic = time.time()
    while True:
        count_of_frames += 1
        if cv2.getWindowProperty(WINDOW_NAME, 0) < 0:
            break
        img = cam.read()
        if img is None:
            break
        # boxes, confs, clss = trt_yolo.detect(img, conf_th)
        boxes, confs, clss = image_detection(img, trt_yolo, conf_th)
        img = vis.draw_bboxes(img, boxes, confs, clss)
        img = show_fps(img, fps)
        # img = draw_door(frame, doors)
        # trackers = tracker.update(detects)
        # img = track_and_draw_and_write(trackers, frame, out)

        cv2.imshow(WINDOW_NAME, img)
        toc = time.time()
        curr_fps = 1.0 / (toc - tic)
        # calculate an exponentially decaying average of fps number
        fps = curr_fps if fps == 0.0 else (fps * 0.95 + curr_fps * 0.05)
        tic = toc
        print(fps)
        key = cv2.waitKey(1)
        if key == 27:  # ESC key: quit program
            break
        elif key == ord("F") or key == ord("f"):  # Toggle fullscreen
            full_scrn = not full_scrn
            set_display(WINDOW_NAME, full_scrn)
    print("--- %s seconds ---" % (time.time() - start_time))
    print("--- %s FPS ---" % (count_of_frames / (time.time() - start_time)))


def main():
    args = parse_args()
    if args.category_num <= 0:
        raise SystemExit("ERROR: bad category_num (%d)!" % args.category_num)
    if not os.path.isfile("yolo/%s.trt" % args.model):
        raise SystemExit("ERROR: file (yolo/%s.trt) not found!" % args.model)

    cam = Camera(args)
    if not cam.isOpened():
        raise SystemExit("ERROR: failed to open camera!")

    cls_dict = get_cls_dict(args.category_num)
    yolo_dim = args.model.split("-")[-1]
    if "x" in yolo_dim:
        dim_split = yolo_dim.split("x")
        if len(dim_split) != 2:
            raise SystemExit("ERROR: bad yolo_dim (%s)!" % yolo_dim)
        w, h = int(dim_split[0]), int(dim_split[1])
    else:
        h = w = int(yolo_dim)
    if h % 32 != 0 or w % 32 != 0:
        raise SystemExit("ERROR: bad yolo_dim (%s)!" % yolo_dim)

    trt_yolo = TrtYOLO(args.model, (h, w), args.category_num)

    open_window(WINDOW_NAME, "Camera TensorRT YOLO Demo", cam.img_width, cam.img_height)
    vis = BBoxVisualization(cls_dict)
    tracker = sort.Sort(5)
    loop_and_detect(cam, trt_yolo, tracker, conf_th=0.3, vis=vis)

    cam.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
